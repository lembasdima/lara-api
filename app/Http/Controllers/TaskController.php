<?php

namespace App\Http\Controllers;

use App\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TaskController extends Controller
{

    public function showAllTasks()
    {
        $tasks = Task::where('user_id', Auth::id())->get();

        return response()->json($tasks, 201);
    }

    public function showSingleTask(Task $task)
    {
        if($task->user()->first()->id == Auth::id())
        {
            return response()->json($task, 201);
        }
        return response()->json(['error' => 'Not found', 404]);
    }

    public function store(Request $request)
    {
        $task = new Task($request->all());
        $task->user_id = $request->user()->id;
        $task->saveOrFail();

        return response()->json($task, 201);
    }

    public function update(Request $request, Task $task)
    {
        if(Auth::id() == $task->user()->first()->id) {
            $task->update($request->all());

            return response()->json($task, 201);
        }
        return response()->json(['error' => 'Not found', 404]);
    }

    public function delete(Task $task)
    {
        if(Auth::id() == $task->user()->first()->id)
        {
            $task->delete();
            return response()->json('Task was deleted', 204);
        }
        return response()->json(['error' => 'Not found', 404]);
    }
}
